//
//  KineduEndpoints.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 23/07/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation

import Foundation
import Moya

enum Liverpool {
    case getProducts(search:String, page:Int, items: Int)
}

extension Liverpool: TargetType {
    
    var baseURL: URL { return URL(string: "https://shoppapp.liverpool.com.mx/appclienteservices/services/v3/plp")! }
    
    var path: String {
        switch self {
        case .getProducts:
            return ""
        }
    }
    
    var headers: [String : String]? {
        switch self {
        case .getProducts:
            return ["Content-Type":"application/json"]
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getProducts:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
        case .getProducts(let search, let page, let items):
            return .requestParameters(parameters: ["force-plp":true, "search-string":search, "page-number":page, "number-of-items-per-page":items], encoding: URLEncoding.queryString)
        }
    }
}
