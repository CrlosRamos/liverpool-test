//
//  Manager.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 23/07/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation
import Moya

class Manager {
    
    private let provider = MoyaProvider<Liverpool>()
    
    func getProducts(search:String, page:Int, items:Int ,completion: @escaping ([Product]?, Error?) -> ()) {
        provider.request(.getProducts(search: search, page: page, items: items)) { result in
            switch result {
            case .success(let response):
                let decoder = JSONDecoder()
                do {
                    let products = try decoder.decode(plpResults.self, from: response.data)
                    completion(products.plpResults.records, nil)
                } catch let error {
                    completion(nil, error)
                }
            case .failure(let error):
                print("Error request: \(error.errorDescription ?? "")")
                completion(nil, error)
            }
        }
    }
}

