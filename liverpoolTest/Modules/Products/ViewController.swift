//
//  ViewController.swift
//  liverpoolTest
//
//  Created by Carlos Ramos on 23/07/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    let manager = Manager()
    var products = [Product]()
    var searchText = ""
    var page = 1
    
    let searchView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let searchFiedl: UITextField = {
        let field = UITextField()
        field.translatesAutoresizingMaskIntoConstraints = false
        field.placeholder = "Search"
        field.autocorrectionType = .no
        return field
    }()
    
    lazy var tableView: UITableView = {
        let table = UITableView()
        table.translatesAutoresizingMaskIntoConstraints = false
        table.register(ProductCell.self, forCellReuseIdentifier: "cell")
        table.delegate = self
        table.dataSource = self
        return table
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        getProducts(page:page,searchString:"computadora")
    }
    
    private func getProducts(page:Int,searchString:String){
        manager.getProducts(search: searchString, page: page, items: 20) { products, error in
            if let result = products{
                if page > 1 {
                    self.products += result
                }else {
                    self.products = result
                }
                self.tableView.reloadData()
            }else{
                print(error ?? "")
            }
        }
    }
    
    private func setupView(){
        hideKeyboardWhenTappedAround()
        searchFiedl.delegate = self
        view.backgroundColor = .systemBackground
        view.addSubview(searchView)
        view.addSubview(tableView)
        searchView.addSubview(searchFiedl)
        
        searchView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        searchView.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor).isActive = true
        searchView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        searchView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.1).isActive = true
        
        searchFiedl.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 30).isActive = true
        searchFiedl.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -30).isActive = true
        searchFiedl.topAnchor.constraint(equalTo: searchView.topAnchor, constant: 20).isActive = true
        searchFiedl.bottomAnchor.constraint(equalTo: searchView.bottomAnchor, constant: -20).isActive = true
        
        tableView.leadingAnchor.constraint(equalTo: view.leadingAnchor).isActive = true
        tableView.topAnchor.constraint(equalTo: searchView.bottomAnchor).isActive = true
        tableView.trailingAnchor.constraint(equalTo: view.trailingAnchor).isActive = true
        tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor).isActive = true
    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return products.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as? ProductCell else { return UITableViewCell() }
        cell.populate(product: products[indexPath.row])
        return cell
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let currentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height

        if maximumOffset - currentOffset <= 100.0 {
            page = page + 1
            self.getProducts(page: page, searchString: searchFiedl.text ?? "")
        }
    }
    
}

extension ViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if let char = string.cString(using: String.Encoding.utf8) {
            let isBackSpace = strcmp(char, "\\b")
            if (isBackSpace == -92) {
                searchText.removeLast()
            }else{
                searchText = "\(textField.text ?? "")\(string)"
            }
        }
        getProducts(page: 1, searchString: searchText)
        return true
    }
    
}
