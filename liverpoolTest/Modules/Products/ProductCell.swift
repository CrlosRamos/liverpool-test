//
//  ProductCell.swift
//  liverpoolTest
//
//  Created by Carlos Ramos on 23/07/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import UIKit

class ProductCell: UITableViewCell {
    
    private var productImage: ImageLoader = {
        let iv = ImageLoader()
        iv.translatesAutoresizingMaskIntoConstraints = false
        iv.contentMode = .scaleAspectFit
        return iv
    }()
    
    private let productName: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        label.textAlignment = .center
        label.numberOfLines = 0
        label.text = "Name"
        return label
    }()
    
    private let productPrice: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 16, weight: .semibold)
        label.text = "Name"
        return label
    }()
    
    private let productSeller: UILabel = {
        let label = UILabel()
        label.translatesAutoresizingMaskIntoConstraints = false
        label.font = UIFont.systemFont(ofSize: 14, weight: .semibold)
        label.text = "Name"
        return label
    }()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setupView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupView(){
        selectionStyle = .none
        addSubview(productImage)
        addSubview(productName)
        addSubview(productPrice)
        addSubview(productSeller)
        
        productImage.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 20).isActive = true
        productImage.centerYAnchor.constraint(equalTo: centerYAnchor).isActive = true
        productImage.widthAnchor.constraint(equalToConstant: 90).isActive = true
        productImage.heightAnchor.constraint(equalToConstant: 90).isActive = true
        
        productName.topAnchor.constraint(equalTo: productImage.topAnchor, constant: 10).isActive = true
        productName.leadingAnchor.constraint(equalTo: productImage.trailingAnchor, constant: 10).isActive = true
        productName.trailingAnchor.constraint(equalTo: trailingAnchor,constant: -10).isActive = true
        
        productPrice.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -20).isActive = true
        productPrice.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
        
        productSeller.leadingAnchor.constraint(equalTo: productName.leadingAnchor, constant: 0).isActive = true
        productSeller.bottomAnchor.constraint(equalTo: bottomAnchor, constant: -8).isActive = true
    }
    
    func populate(product: Product){
        if let strUrl = product.smImage.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
              let imgUrl = URL(string: strUrl) {
              productImage.loadImageWithUrl(imgUrl)
        }
        productName.text = product.productDisplayName
        productPrice.text = "$\(product.listPrice)"
        productSeller.text = product.seller
        
    }
    
}
