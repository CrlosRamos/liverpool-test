//
//  NPSScore.swift
//  kinedu-test
//
//  Created by Carlos Ramos on 23/07/20.
//  Copyright © 2020 Carlos Ramos. All rights reserved.
//

import Foundation

struct plpResults:Decodable {
    let plpResults: Records
    
    private enum CodingKeys: String, CodingKey { case plpResults}
      
      init(from decoder: Decoder) throws {
          let container = try decoder.container(keyedBy: CodingKeys.self)
          plpResults = try container.decode(Records.self, forKey: .plpResults)
      }
}

struct Records: Decodable {
    let records: [Product]
    private enum CodingKeys: String, CodingKey { case records}
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        records = try container.decode([Product].self, forKey: .records)
    }
}

struct Product:Decodable {
    let productDisplayName: String
    let listPrice: Double
    let seller: String
    let smImage: String
    
    private enum CodingKeys: String, CodingKey { case productDisplayName,listPrice, seller,smImage }
    
    init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        productDisplayName = try container.decode(String.self, forKey: .productDisplayName)
        listPrice = try container.decode(Double.self, forKey: .listPrice)
        seller = try container.decode(String.self, forKey: .seller)
        smImage = try container.decode(String.self, forKey: .smImage)
    }
}
